package leelawattananonkul.arisara.lab7;

/**
 * @author  Arisara Leelawattananonkul
 * @593040687-9
 * @since   2018-03-10
 */

import java.awt.*;
import javax.swing.*;

public class AthleteFormV2 extends AthleteFormV1 {

	private static final long serialVersionUID = -893034230016236558L;
	protected JLabel gender, competition;
	protected JTextArea competText;
	protected JRadioButton maleButt, femaleButt;
	protected ButtonGroup buttonGroup;
	protected JPanel genderPanel, mfButtPanel, mainGenderPanel, competPanel, subV2Panel, v2Panel;
	protected JScrollPane competScroll;
	
	public AthleteFormV2(String title) {
			super(title);
	}

	public static void createAndShowGUI(){
		AthleteFormV2 AthleteForm2 = new AthleteFormV2("Athlete Form V2");
		AthleteForm2.addComponents();
		AthleteForm2.setFrameFeatures();
	}
	
	protected void addComponents() {
		super.addComponents();
		gender = new JLabel("Gender : ");
		femaleButt = new JRadioButton("Female");
		maleButt = new JRadioButton("Male", true);		
		buttonGroup = new ButtonGroup(); 
		buttonGroup.add(maleButt);
		buttonGroup.add(femaleButt);
		genderPanel = new JPanel(new GridLayout(1,2));
		mfButtPanel = new JPanel(new GridLayout(1,2));
		mfButtPanel.add(maleButt);
		mfButtPanel.add(femaleButt);
		genderPanel.add(gender);
		genderPanel.add(mfButtPanel);
		
		competition = new JLabel("Competition : ");
		competText = new JTextArea(2,35); 
		competScroll = new JScrollPane(competText); 
		competText.setLineWrap(true); 
		competPanel = new JPanel(new BorderLayout());
		competPanel.add(competition, BorderLayout.NORTH);
		competPanel.add(competScroll, BorderLayout.CENTER);
		
		subV2Panel = new JPanel(new BorderLayout());
		subV2Panel.add(genderPanel, BorderLayout.NORTH);
		subV2Panel.add(competPanel, BorderLayout.CENTER);
		
		v2Panel = new JPanel();
		v2Panel.add(subV2Panel);
		v1Panel.add(v2Panel, BorderLayout.CENTER);
		
		superV1Panel.add(v1Panel);
		this.setContentPane(superV1Panel);
		
	}

	protected void setFrameFeatures() {
		super.setFrameFeatures();

	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
