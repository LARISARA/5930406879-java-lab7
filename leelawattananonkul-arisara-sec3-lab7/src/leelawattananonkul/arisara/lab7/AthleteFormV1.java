package leelawattananonkul.arisara.lab7;

/**
 * @author  Arisara Leelawattananonkul
 * @593040687-9
 * @since   2018-03-10
 */

import java.awt.*;
import javax.swing.*;

public class AthleteFormV1 extends MySimpleWindow {

	private static final long serialVersionUID = -6089339843954226351L;
	protected JLabel name, birthdate, weight, height, nationality;
	protected JTextField nameText, birthText, weightText, heightText, nationText;
	protected JPanel panel, namePanel, birthPanel, weightPanel, heightPanel, nationPanel, v1Panel, superV1Panel;

	public AthleteFormV1(String title) {
		super(title);
	}

	public static void createAndShowGUI() {
		AthleteFormV1 athleteForm1 = new AthleteFormV1("Athlete Form V1");
		athleteForm1.addComponents();
		athleteForm1.setFrameFeatures();
	}

	protected void addComponents() {
		super.addComponents();
		name = new JLabel("Name : ");
		birthdate = new JLabel("Birthdate : ");
		weight = new JLabel("Weight(kg.) : ");
		height = new JLabel("Height(metre) : ");
		nationality = new JLabel("Nationality : ");
		
		int textLenght = 15;
		nameText = new JTextField(textLenght);
		birthText = new JTextField(textLenght);
		//When users place the cursor at this field, the program shows the tooltip text as �ex. 22.02.2000�
		birthText.setToolTipText("ex. 22.02.2000");
		weightText = new JTextField(textLenght);
		heightText = new JTextField(textLenght);
		nationText = new JTextField(textLenght);
		
		namePanel = new JPanel(new GridLayout(1,2));
		birthPanel = new JPanel(new GridLayout(1,2));
		weightPanel = new JPanel(new GridLayout(1,2));
		heightPanel = new JPanel(new GridLayout(1,2));
		nationPanel = new JPanel(new GridLayout(1,2));
		
		namePanel.add(name);
		namePanel.add(nameText);
		birthPanel.add(birthdate);
		birthPanel.add(birthText);
		weightPanel.add(weight);
		weightPanel.add(weightText);
		heightPanel.add(height);
		heightPanel.add(heightText);
		nationPanel.add(nationality);
		nationPanel.add(nationText);
		
		panel = new JPanel(new GridLayout(5,1));
		panel.add(namePanel);
		panel.add(birthPanel);
		panel.add(weightPanel);
		panel.add(heightPanel);
		panel.add(nationPanel);
		
		v1Panel = new JPanel(new BorderLayout());
		v1Panel.add(panel, BorderLayout.NORTH);
		v1Panel.add(simplePanel, BorderLayout.SOUTH);
		
		superV1Panel = new JPanel();
		superV1Panel.add(v1Panel);
		this.setContentPane(superV1Panel);
	}

	protected void setFrameFeatures() {
		super.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
