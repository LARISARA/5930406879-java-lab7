package leelawattananonkul.arisara.lab7;

/**
 * @author  Arisara Leelawattananonkul
 * @593040687-9
 * @since   2018-03-10
 */

import java.awt.*;
import javax.swing.*;

public class MySimpleWindow extends JFrame {

	private static final long serialVersionUID = -5897468282291075818L;
	protected JButton cancelButt, okButt;
	protected JPanel simplePanel;
	
	public MySimpleWindow(String title) {
		super(title);
	}

	public static void createAndShowGUI() {
		MySimpleWindow msw = new MySimpleWindow("My Simple Window");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	protected void addComponents() {
		cancelButt = new JButton("Cancel");
		okButt = new JButton("OK");
		simplePanel = new JPanel(new FlowLayout());
		// add Cancel and Ok Button to simplePanal
		simplePanel.add(cancelButt);
		simplePanel.add(okButt);
		this.setContentPane(simplePanel);
	}

	protected void setFrameFeatures() {
		pack();
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int width = this.getWidth();
		int height = this.getHeight();
		int x = (dim.width-width)/2;
		int y = (dim.height-height)/2;
		setLocation(x,y);
		setDefaultLookAndFeelDecorated(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);		
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
