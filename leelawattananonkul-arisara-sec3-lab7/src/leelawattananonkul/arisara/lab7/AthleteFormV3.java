package leelawattananonkul.arisara.lab7;

/**
 * @author  Arisara Leelawattananonkul
 * @593040687-9
 * @since   2018-03-10
 */

import java.awt.*;
import javax.swing.*;

public class AthleteFormV3 extends AthleteFormV2 {

	private static final long serialVersionUID = 6376584172107792365L;
	protected JMenuBar menuBar;
	protected JMenu fileMenu, configMenu;
	protected JMenuItem newItem, openItem, saveItem, exitItem, colorItem, sizeItem;
	protected JLabel type;
	protected JComboBox<String> typeCombo;
	protected JPanel typePanel;
	
	public AthleteFormV3(String title) {
			super(title);
	}
	public static void createAndShowGUI(){
		AthleteFormV3 AthleteForm3 = new AthleteFormV3("Athlete Form V3");
		AthleteForm3.addComponents();
		AthleteForm3.setFrameFeatures();
	}
	
	protected void addComponents() {
		super.addComponents();
		// Add MenuBar
		menuBar = new JMenuBar();
		
		//Add Menu
		fileMenu = new JMenu("File");
		configMenu = new JMenu("Config");
		
		//Add MenuItem
		newItem = new JMenuItem("New");
		openItem = new JMenuItem("Open");
		saveItem = new JMenuItem("Save");
		exitItem = new JMenuItem("Exit");
		colorItem = new JMenuItem("Color");
		sizeItem = new JMenuItem("Size");
		
		//Add MenuItem for each Menu
		fileMenu.add(newItem);
		fileMenu.add(openItem);
		fileMenu.add(saveItem);
		fileMenu.add(exitItem);
		configMenu.add(colorItem);
		configMenu.add(sizeItem);
		
		//Add Menu to MenuBar
		menuBar.add(fileMenu);
		menuBar.add(configMenu);
		setJMenuBar(menuBar);
		
		//this program use JComboBox to let users see all choices in column.
		type = new JLabel("Type : ");
		typeCombo = new JComboBox<String>();
		typeCombo.setPreferredSize(new Dimension(210, 25)); 
		typeCombo.addItem("Badminton player");
		typeCombo.addItem("Boxer"); 
		typeCombo.addItem("Footballer");
		typeCombo.setEditable(true); // users can make a new choice from JComboBox
		
		typePanel = new JPanel(new BorderLayout());
		typePanel.add(typeCombo, BorderLayout.EAST);
		typePanel.add(type, BorderLayout.WEST);
		
		competPanel.add(typePanel, BorderLayout.SOUTH);
		
		this.setContentPane(superV1Panel);
		
	}

	protected void setFrameFeatures() {
		super.setFrameFeatures();

	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	
}
